/*
  b2ev - A lightweight batch to executable toolkit.
  Copyright (C) 2019 Teddy ASTIE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <windows.h>

/* "no window" variant of _spawnvp */

static intptr_t spawnvp_no_window(int mode, const char *cmdname, const char *const *argv)
{
  size_t cmd_length = 0;
  size_t offset = 0;

  #ifdef DEBUG
  puts("In spawnvp_no_window\n");
  #endif

  int i = 0;
  while (argv[i]) {
    cmd_length += strlen(argv[i]) + 1;

    #ifdef DEBUG
    printf("cmd_length: %lu\n"
           "argv[i] = %s\n\n",
           cmd_length, argv[i]);
    #endif

    i++;
  }

  char *cmdline = calloc(1, cmd_length + 1);
  if (!cmdline)
    return -1;

  i = 0;
  while (argv[i]) {
    size_t len = strlen(argv[i]);
    memcpy(cmdline + offset, argv[i], len);

    offset += len;

    #ifdef DEBUG
    printf("len: %lu\n"
           "offset = %lu\n"
           "argv[i] = %s\n"
           "cmdline = %s\n\n",
           len, offset, argv[i], cmdline);
    #endif


    cmdline[offset] = ' ';

    offset++;
    i++;
  }

  #ifdef DEBUG
  printf("final cmdline: %s\n\n", cmdline);
  #endif

  STARTUPINFOA si;
  PROCESS_INFORMATION pi;

  memset(&si, '\0', sizeof(STARTUPINFOA));
  memset(&pi, '\0', sizeof(PROCESS_INFORMATION));

  #ifdef DEBUG
  puts("Let's go !");
  #endif

  bool status = CreateProcessA(argv[0], cmdline, NULL, NULL,
    false, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);

  free(cmdline);

  if (!status) {
    #ifdef DEBUG
    printf("Failed to create process (%x)", GetLastError());
    #endif

    return -1;
  }

  /* Wait for process termination. */
  WaitForSingleObject(pi.hProcess, INFINITE);

  DWORD code;
  GetExitCodeProcess(pi.hProcess, &code);

  CloseHandle(pi.hProcess);
  CloseHandle(pi.hThread);

  return code;
}
