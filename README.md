```
 _    ___
| |__|_  )_____ __
| '_ \/ // -_) V /
|_.__/___\___|\_/
```

# Bat2Exe eVolved
_Lightweight toolkit to pack batch files (and even directories) to a single autonomous executable._

## Features
 - very lightweight and fast executable
 - lz4 compression (with in-memory uncompression)
 - directory packing
 - modular approach
 - custom non-Windows platform support

## How to use
You need a working [Tiny C Compiler](https://bellard.org/tcc/) either in `tcc` directory or in your PATH.

Put all your files in `files` directory then run build.bat, your ready executable is **output.exe**.
Your files must contains main.bat which is the entry point of your program.

## Advanced use
You can modify some parameters in build.bat.

| Parameter  | Usage
| ---------- | ------------------------------------------------------------------------------------------------------------------- |
| USE_LZ4    | Enable/Disable lz4 usage on internal payload, disabling it could be useful to improve UPX efficiency on executable. |
| LZ4_FAST   | Use default profile when compressing payload, trading space for build speed.                                        |
| NO_CONSOLE | Close the console at the very beginning of the executable. With NO_CONSOLE_FORCE, disable the console. Windows only.|
| DEBUG      | Write debug informations, only useful when debugging b2ev.c.                                                        |

## Advanced use (other platforms (POSIX))
Currently, on other platforms, you can't build executables easily.
You have to reproduce the behavior of build.bat on other platforms with your own compiler

You need a working [Dos9](http://dos9.org/) executable in `files` directory.

For example :
 1. Run `build.bat` on Windows
 2. Put all your files WITH an additional main.sh file and a working dos9 executable.
    Your main.sh should run main.bat using dos9.
    e.g
    ```
    #!/bin/sh
    ./dos9 main.bat $*
    ```
 3. Use WSL and build output :
  ```
  cc -o output -O2 -s src/b2ev.c src/lib/lz4.c src/lib/microtar.c
  ```

## Technical explanation
When you wan't to build your executable, b2ev work with 2 stages :

#### First stage : building
 1. Build required tools (using tcc by default).
 2. `bin/dir2tar` builds a [tarball](https://en.wikipedia.org/wiki/Tar_(computing)) from `files` directory, this tarball is `packed.tar` (archive readable by most archive managers)
 3. `bin/bin2c` from `packed.tar` builds `src/packed.h` which is the payload (compressed to lz4 if `USE_LZ4` is on) converted to a header file, included in b2ev.c.
 4. Using compiler (tcc by default), build `src/b2ev.c` (using proper libaries) including `packed.h`, b2ev.c contains the output code.
After this stage, you get output.exe, your executable.

#### Second stage : unpacking
When you have your executable, you need to unpack it to run your batch file.
 1. If needed (lz4 compression), uncompress payload.
 2. Take a available unpacking directory such as `(TEMP)/b2ev_(hex number)`
    (hex number) is the least number of a `(TEMP)/b2ev_(hex number)` directory that does not exist.
    NOTE: It's more reliable than random number that can have collisions (e.g overwriting currently used b2ev directory).
 3. Unpack the tarball (payload) to this directory.
 4. Run **main.bat** (or **main.sh** on other platforms).
 5. Cleans unpacking directory.

## Used libraries

[lz4](https://lz4.github.io/lz4/) by Yann Collet
[microtar](https://github.com/rxi/microtar) by rxi
[bin2c](https://github.com/gwilymk/bin2c) by Gwilym Kuiper (modified by Teddy Astie)

## Licence

  b2ev - A lightweight batch to executable toolkit.
  Copyright (C) 2018-2019 Teddy ASTIE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
