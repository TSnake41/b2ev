/*
  b2ev - A lightweight batch to executable toolkit.
  Copyright (C) 2018-2019 Teddy ASTIE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* Embedded code */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "lib/microtar.h"
#include "packed.h"

#ifdef pack_LZ4
#include "lib/lz4.h"
#endif

#ifndef WIN32
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#else
#include <windows.h>
#include <direct.h>
#include <process.h>

#define chdir _chdir
#define rmdir _rmdir
#define remove DeleteFileA

#define mkdir(path, mode) (!CreateDirectoryA(path, NULL))
#endif

#if defined(NO_CONSOLE) && defined(WIN32)
#include "spawnvp_nowin.c"
#define _spawnvp spawnvp_no_window
#endif

struct mtar_buffer {
  const void *b;
  size_t s;
};

static int mtar_buffer_read(mtar_t *tar, void *data, unsigned int size)
{
  struct mtar_buffer *stream = tar->stream;
  
  #ifdef DEBUG
  printf("(mtar_buffer_read): stream->s = %lu\n"
         "(mtar_buffer_read): tar->pos = %u\n"
         "(mtar_buffer_read): size = %u\n"
         "(mtar_buffer_read): (stream->s - tar->pos >= size) = %u\n"
         "(mtar_buffer_read): (stream->b + tar->pos) = %p\n\n",
         stream->s, tar->pos, size, stream->s - tar->pos >= size, stream->b + tar->pos);
  #endif
  
  if (stream->s - tar->pos >= size) {
    memcpy(data, stream->b + tar->pos, size);
    return MTAR_ESUCCESS;
  } else {
    return MTAR_EREADFAIL;
  }
}

static int mtar_buffer_seek(mtar_t *tar, unsigned pos)
{
  /* use internal tar->pos */
  return MTAR_ESUCCESS;
}

static int mtar_buffer_close(mtar_t *tar)
{
  return MTAR_ESUCCESS;
}

int main(int argc, char **argv)
{
  #if defined(WIN32) && defined(NO_CONSOLE) && !defined(NO_CONSOLE_FORCE) && !defined(DEBUG)
  /* Close the console */
  FreeConsole();
  #endif

  /* First uncompress tarball if needed. */
  void *u_tarball;

  #ifdef pack_LZ4
  u_tarball = calloc(1, pack_length_uncompressed);
  if (u_tarball == NULL)
    exit(1);
  
  if (LZ4_decompress_safe(pack, u_tarball, pack_length, pack_length_uncompressed) < 0)
    exit(1);
  
  #ifdef DEBUG
  printf("Compressed mode (%d / %d)\n", pack_length, pack_length_uncompressed);
  #endif

  #else
  /* Luckly, payload is plain. */
  u_tarball = (void *)pack;
  
  #ifdef DEBUG
  puts("No compression.");
  #endif
  #endif
  
  /* Now the payload is ready, unpack it to a temporary directory. */
  const char *temp_dir =
  #ifndef WIN32
    "/tmp";
  #else
    getenv("TMP");
  #endif
  
  /* unpack directory : (temp)/b2ev_XXXX
     So, length = strlen(temp) + 14 */
  
  /* Take a directory that does not exists. */
  char *unpack_dir = calloc(1, strlen(temp_dir) + 14 + 1);
  unsigned short n = 0;
  while (1) {
    snprintf(unpack_dir, strlen(temp_dir) + 15, 
      #ifndef WIN32
      "%s/b2ev_%hx", 
      #else
      "%s\\b2ev_%hx", 
      #endif
      temp_dir, n
    );
  
    if (mkdir(unpack_dir, 0777) == 0)
      /* This directory is available, take it. */
      break;
    
    n++;
  }
  
  #ifdef DEBUG
  printf("unpack_dir = %s\n", unpack_dir);
  #endif
  
  chdir(unpack_dir);
  
  /* The most interesting part : unpack the tarball */
  mtar_t tar;
  memset(&tar, 0, sizeof(mtar_t));
  
  struct mtar_buffer tar_buffer;
  tar_buffer.b = u_tarball;
  
  #ifdef pack_LZ4
  tar_buffer.s = pack_length_uncompressed;
  #else
  tar_buffer.s = pack_length;
  #endif
  
  tar.stream = &tar_buffer;
  
  tar.read = &mtar_buffer_read;
  tar.seek = &mtar_buffer_seek;
  tar.close = &mtar_buffer_close;
  
  #ifdef DEBUG
  printf("tar.read = %p\ntar.seek = %p\ntar.close = %p\ntar.stream = %p\n\n", 
    tar.read, tar.seek, tar.close, tar.stream);
  #endif

  mtar_header_t head;
  
  size_t buffer_size = 0;
  char *buffer = NULL;
  
  while (mtar_read_header(&tar, &head) == MTAR_ESUCCESS) {
    #ifdef DEBUG
    printf("%s: %d\n", head.name, head.type);
    #endif
    
    if (head.type == MTAR_TREG) {
      /* Write file to filesystem. */
      FILE *f = fopen(head.name, "wb");
      if (f == NULL) {
        fprintf(stderr, "FATAL: Cannot open '%s'", head.name);
        exit(1);
      }
      
      if (buffer_size < head.size) {
        /* Buffer is too small, grow it. */
        char *new_buffer = realloc(buffer, head.size);
        if (new_buffer == NULL) {
          fprintf(stderr, "FATAL: Out of memory on file '%s'", head.name);
          exit(1);
        }
        
        buffer = new_buffer;
        buffer_size = head.size;
      }
      
      if (mtar_read_data(&tar, buffer, head.size) == MTAR_EFAILURE) {
        fprintf(stderr, "FATAL: Incomplete data on '%s'", head.name);
        exit(1);
      }
      
      fwrite(buffer, head.size, 1, f);
      fclose(f);
      
      #ifndef WIN32
      chmod(head.name, 0777);
      #endif
    } else if (head.type == MTAR_TDIR) {
      if (mkdir(head.name, 0777) != 0) {
        fprintf(stderr, "FATAL: Can't create directory '%s'", head.name);
        exit(1);
      }
    }
    
    mtar_next(&tar);
  }
  
  free(buffer);
  
  /* Okay, now, we're good, just run main.bat (or main.sh on Unix platforms). */
  /* Reuse argv. */
  #ifdef DEBUG
  #ifndef WIN32
  puts("Run main.sh\n----------");
  #else
  puts("Run main.bat\n----------");
  #endif
  #endif
  
  #ifndef WIN32
  pid_t pid = fork();
  int code;
  
  if (pid) {
    /* Parent
       Wait for child.
    */
    waitpid(pid, &code, 0);
  } else {
    /* Child */
    argv[0] = "./main.sh";
    execvp(argv[0], argv);
    exit(0);
  }
  #else
  argv[0] = "main.bat";
  intptr_t code = _spawnvp(_P_WAIT, argv[0], argv);
  #endif
  
  #ifdef DEBUG
  puts("----------");
  
  printf("error code = %d\n\n", code);
  #endif

  /* Do some cleanup next. */
  #ifdef DEBUG
  puts("Cleanup...");
  #endif

  mtar_rewind(&tar);
  
  while (mtar_read_header(&tar, &head) == MTAR_ESUCCESS) {
    #ifdef DEBUG
    printf("Delete %s\n", head.name);
    #endif
    
    if (head.type == MTAR_TREG)
      /* Delete file. */
      remove(head.name);
    else if (head.type == MTAR_TDIR)
      /* Delete directory */
      rmdir(head.name);

    mtar_next(&tar);
  }
  
  mtar_close(&tar);
  
  #ifdef pack_LZ4
  free(u_tarball);
  #endif
  
  #ifdef DEBUG
  printf("Delete %s\n", unpack_dir);
  #endif
  
  chdir("..");
  rmdir(unpack_dir);
  free(unpack_dir);
  
  #ifdef DEBUG
  puts("Bye world !");
  #endif
  
  /* Terminate b2ev. */
  return code;
}