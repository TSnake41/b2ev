@Echo off
setlocal enabledelayedexpansion

:: Use lz4 compression algorithm.
:: Disable it if you wan't to use UPX instead of embedded compression.
set USE_LZ4=1

:: (needs USE_LZ4)
:: Use lz4 default compression mode instead of
:: high-compression mode thus builds a lot faster 
:: but makes the executable slightly bigger.
set LZ4_FAST=0

:: Close the console at the very beginning of the executable.
set NO_CONSOLE=0

:: (needs NO_CONSOLE)
:: Force the console to don't show at all.
:: This option is discouraged as it makes the executable
:: significantly more suspicious to anti-viruses.
set NO_CONSOLE_FORCE=0

:: Debug mode.
set DEBUG=0

set PATH=%CD%\tcc;%PATH%

set "defs="

for %%A in (USE_LZ4 LZ4_FAST DEBUG NO_CONSOLE) do (
  if !%%A!==1 (
    set "defs=!defs! -D%%A"
  )
)

if !NO_CONSOLE_FORCE!==1 (
  if !NO_CONSOLE!==1 (
    if !DEBUG!==0 (
      set "defs=-Wl,-subsystem=gui !defs!"
    )
  )
)

echo on

mkdir bin 2>nul

tcc %defs% src/dir2tar.c src/lib/microtar.c -o bin/dir2tar.exe
tcc %defs% src/bin2c.c src/lib/lz4.c src/lib/lz4hc.c -o bin/bin2c.exe

bin\dir2tar files packed.tar
bin\bin2c packed.tar src/packed.h pack
tcc %defs% src/b2ev.c src/lib/lz4.c src/lib/microtar.c -o output.exe

pause