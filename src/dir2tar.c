/*
  b2ev - A lightweight batch to executable toolkit.
  Copyright (C) 2018 Teddy ASTIE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* tar pack creator */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>

#include "lib/microtar.h"

#ifndef WIN32
#include <unistd.h>
#include <dirent.h>
#else
#include <windows.h>
#include "lib/dirent.h"

#define chdir SetCurrentDirectory
#endif

void explore_dir(mtar_t *tar, char *prefix, size_t prefix_size);
void *strdup_csize(void *buffer, size_t buffer_size, size_t new_size);

int main(int argc, char **argv)
{
  mtar_t tar;
  
  if (argc < 3) {
    fputs("Usage : dir2tar path pack.tar", stderr);
    return 0;
  }
  
  char *path = argv[1];
  char *output = argv[2];
  
  mtar_open(&tar, output, "w");
    
  chdir(path);

  /* Internal prefix buffer. */
  /* TODO: Avoid '.' as root. */
  explore_dir(&tar, ".", 2);
  
  mtar_finalize(&tar);
  mtar_close(&tar);
}

void *strdup_csize(void *buffer, size_t buffer_size, size_t new_size)
{
  void *new_buffer = calloc(new_size, 1);
  if (new_buffer == NULL)
    return NULL;
 
  return memcpy(new_buffer, buffer, buffer_size);
}

#ifdef WIN32
FILE *fopen_win(const char *_path, const char *mode)
{
  /* fopen wrapper to add support to '/' path separator. */
  size_t path_len = strlen(_path);
  
  char *path = calloc(path_len + 1, 1);
  if (path == NULL)
    return NULL;
  
  strncpy(path, _path, path_len);
  
  char *chr;
  /* Replace '/' by '\' */
  while ((chr = strchr(path, '/')) != NULL)
    *chr = '\\';
  
  /* Finally */
  FILE *f = fopen(path, mode);
  free(path);
  return f;
}

#define fopen fopen_win
#endif

void explore_dir(mtar_t *tar, char *prefix, size_t prefix_size)
{
  DIR *dir = opendir(prefix);
  struct dirent *ent;

  if (dir == NULL) {
    fputs("path not found", stderr);
    return;
  }
  
  fprintf(stderr, "[%s/]*\n", prefix);
  
  while ((ent = readdir(dir)) != NULL) {
    if (ent->d_type == DT_DIR) {
      
      if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0)
        continue;
      
      size_t new_prefix_size = prefix_size + strlen(ent->d_name) + 1;
      
      /* NOTE: prefix_size includes NUL-terminator. */
      char *new_prefix = strdup_csize(prefix, prefix_size, new_prefix_size);
      snprintf(new_prefix, new_prefix_size, "%s/%s", prefix, ent->d_name);
      
      fprintf(stderr, "[%s/]+\n", new_prefix);
      mtar_write_dir_header(tar, new_prefix);
      
      explore_dir(tar, new_prefix, new_prefix_size);
      free(new_prefix);
    } else if (ent->d_type == DT_REG) {
      size_t filename_size = prefix_size + strlen(ent->d_name) + 1;
      char *filename = strdup_csize(prefix, prefix_size, filename_size);
      
      snprintf(filename, filename_size, "%s/%s", prefix, ent->d_name);
      
      FILE *f = fopen(filename, "rb");
      if (f == NULL) {
        /* Ignore this file */
        fprintf(stderr, "[%s]- Unable to read file (%s).\n", filename, strerror(errno));
        free(filename);
        continue;
      }
      
      fseek(f, 0, SEEK_END);
      long int size = ftell(f);
      rewind(f);
            
      void *buffer = calloc(size, 1);
      if (buffer == NULL) {
        fprintf(stderr, "[%s]- File too big. FIXME: use fragmented read/write\n", filename);
        free(filename);
        continue;
      }
      
      fread(buffer, size, 1, f);
      
      fprintf(stderr, "[%s]+\n", filename);
      mtar_write_file_header(tar, filename, (unsigned)size);
      mtar_write_data(tar, buffer, (unsigned)size);

      fclose(f);
      free(buffer);
      free(filename);
    }
  }
}